# encoding: utf-8

module Linkedlist  

	# Clase Nodo
	class Node

		attr_accessor :value, :next_node
  
    	def initialize(value, next_node = nil)
	        @value = value
	        @next_node = next_node
    	end
    
	end
  
	# Clase Referencia
	class Referencia
	
		attr_reader :autores, :titulo, :serie, :editorial, :edicion, :fecha_publicacion, :numeros_isbn
		
		def initialize(autores, titulo, serie, editorial, edicion, fecha_publicacion, numeros_isbn)
			@autores, @titulo, @serie, @editorial, @edicion, @fecha_publicacion, @numeros_isbn = autores, titulo, serie, editorial, edicion, fecha_publicacion, numeros_isbn
		end
		
		def to_s
			# Formato IEEE: Autores, Titulo del libro, Edicion. Lugar de publicación: Editorial, Año de publicación.
			# No se incluye el lugar de publicación porque no está entre los atributos
			s = ''
			@autores.each() { |a| s << a << ', '}
			s << "#{@titulo}, #{@edicion}ª edición. #{@editorial}, #{@fecha_publicacion}"
			return s
		end
		
	end
	
	# Clase Lista
	class List
		
		# Nodos cabeza/cola y tamaño
		attr_reader :head, :tail, :size
		
		def initialize()
			@head = nil
			@tail = nil
			@size = 0
		end
		
		# ¿Está la lista vacía?
		def empty?()
			return (@head == nil)
		end
		
		# Insertar en lista vacía
		def push_empty(ref)
			
			raise RuntimeError, "[List.push_empty]: Lista no vacía" unless empty?()
			
			nodo = Node.new(ref)
			@head = nodo
			@tail = nodo
			@size += 1
			
			return self
			
		end
		
		# Insertar por el final
		def push(ref)
			if empty?()
				return push_empty(ref)
			else
				nodo = Node.new(ref)
				@tail.next_node = nodo
				@tail = nodo
				@size += 1
				return self
			end
		end
		
		# Insertar múltiples elementos por el final
		def push_multi(*refs)
			refs.each { |ref| push(ref)}
		end
		
		# Extraer por el principio
		def pop()
			
			raise RuntimeError, "[List.pop]: No se puede extraer elementos de una lista vacía" if empty?()
			
			nodo = @head
			if @head.equal?(@tail)
				@head = nil
				@tail = nil
			else
				@head = @head.next_node
			end
			
			@size -= 1
			
			return nodo.value
			
		end
		
		private :push_empty
		
	end
  
end
