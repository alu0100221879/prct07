# encoding: utf-8

require "spec_helper"
require "linkedlist"

describe Linkedlist do
	
	before :each do
		
		@nodo = Linkedlist::Node.new("Un valor")		
		
		@r1 = Linkedlist::Referencia.new(['Dave Thomas', 'Andy Hunt', 'Chad Fowler'], 'Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide', '(The Facets of Ruby)', 'Pragmatic Bookshelf', 4, 'July 7, 2013', ['ISBN-13: 978-1937785499', 'ISBN-10: 1937785491'])
		@r2 = Linkedlist::Referencia.new(['Scott Chacon'], 'Pro Git 2009th Edition', '(Pro)', 'Apress', 2009, 'August 27, 2009', ['ISBN-13: 978-1430218333', 'ISBN-10: 1430218339'])
		@r3 = Linkedlist::Referencia.new(['David Flanagan', 'Yukihiro Matsumoto'], 'The Ruby Programming Language', '', 'O’Reilly Media', 1, 'February 4, 2008', ['ISBN-10: 0596516177', 'ISBN-13: 978-0596516178'])
		@r4 = Linkedlist::Referencia.new(['David Chelimsky', 'Dave Astels', 'Bryan Helmkamp', 'Dan North', 'Zach Dennis', 'Aslak Hellesoy'], 'The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends', '(The Facets of Ruby)', 'Pragmatic Bookshelf', 1, 'December 25, 2010', ['ISBN-10: 1934356379', 'ISBN-13: 978-1934356371'])
		@r5 = Linkedlist::Referencia.new('Richard E. Silverman', 'Git Pocket Guide', '', 'O’Reilly Media', 1, 'August 2, 2013', ['ISBN-10: 1449325866', 'ISBN-13: 978-1449325862'])
		
		@l1 = Linkedlist::List.new()
		
	end
	
	describe "Node" do
    	it "# Debe existir un Nodo de la lista con sus datos y su siguiente" do
      		expect(@nodo.value).to eql("Un valor")
      		expect(@nodo.next_node).to eql(nil)
		end
  	end
  	
  	describe "Referencia" do
    	it "# Debe existir una clase Referencia con un método para obtener la referencia formateada" do
      		expect(@r1).to respond_to(:to_s)
		end
  	end
  	
  	describe "List" do
  		
  		it "# Debe existir una clase Lista con su cabeza" do
  			expect(@l1).to be_instance_of(Linkedlist::List)
  			expect(@l1).to respond_to(:head)
  		end
  		
  		it " #Se puede insertar un elemento" do
  			@l1.push(@r1)
  			expect(@l1.head.value).to equal(@r1)
  		end
  		
  		it "# Se pueden insertar varios elementos" do
  			@l1.push_multi(@r1, @r2)
  			expect(@l1.head.value).to equal(@r1)
  			expect(@l1.head.next_node.value).to equal(@r2)
  		end
  		
  		it "# Se puede extraer el primer elemento" do
  			@l1.push(@r1)
  			ref = @l1.pop()
  			expect(ref).to equal(@r1)
  			expect(@l1).to be_empty
  		end
  		
  		it "# Prueba bibliografía" do
  			@l1.push_multi(@r1, @r2, @r3, @r4, @r5)
  			expect(@l1.size).to eql(5)
  		end
  		
  	end
  	
end
